package com.emedinaa.ar;

import java.io.IOException;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.provider.CalendarContract.Events;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.SurfaceView;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;

import com.moodstocks.android.AutoScannerSession;
import com.moodstocks.android.MoodstocksError;
import com.moodstocks.android.Result;
import com.moodstocks.android.Scanner;

import couk.jenxsol.parallaxscrollview.views.ParallaxScrollView;

public class ScanActivity extends Activity implements AutoScannerSession.Listener {

  private static final int TYPES = Result.Type.IMAGE | Result.Type.QRCODE | Result.Type.EAN13;
  private static final String IMAGE01= "img1.png";
  private static final String IMAGE02= "img2.png";
  private static final String IMAGE03= "img3.png";
  
  private AutoScannerSession session = null;
  //SubsamplingScaleImageView imageView;
  ImageView imageView;
  private ParallaxScrollView mScrollView;
  private Button btnExit;
  private Bitmap bmp =null;
  private int heightScreen=0;
  private int widthScreen =0;
  
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_scan);

    SurfaceView preview = (SurfaceView)findViewById(R.id.preview);
    mScrollView = (ParallaxScrollView) findViewById(R.id.scroll_view);
    imageView = (ImageView)findViewById(R.id.imageView1);
    btnExit= (Button)findViewById(R.id.btnExit);

    bmp=null;
    try {
      session = new AutoScannerSession(this, Scanner.get(), this, preview);
      session.setResultTypes(TYPES);
    } catch (MoodstocksError e) {
      e.printStackTrace();
    }
    
    imgEvents();
    
    DisplayMetrics displaymetrics = new DisplayMetrics();
    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    widthScreen =displaymetrics.widthPixels;
    heightScreen =displaymetrics.heightPixels;
    //heightScreen =1024;
    
    btnExit.setVisibility(View.GONE);
    mScrollView.setVisibility(View.GONE);
    
    Log.v("CONSOLE","W-H "+widthScreen+" "+heightScreen);
  }
  private void imgEvents() {
	// TODO Auto-generated method stub
	btnExit.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			// TODO Auto-generated method stub
			if(session!=null)session.resume();
			 btnExit.setVisibility(View.GONE);
			 mScrollView.setVisibility(View.GONE);
		}
	});

}
protected void recycle() {
	// TODO Auto-generated method stub
	if(bmp!=null){bmp.recycle();};
}
private void hideImages() {
}
@Override
  protected void onResume() {
    super.onResume();
    session.start();
  }

  @Override
  protected void onPause() {
    super.onPause();
    session.stop();
  }

  @Override
  public void onCameraOpenFailed(Exception e) {
    // You should inform the user if this occurs!
  }

  @Override
  public void onWarning(String debugMessage) {
    // Useful for debugging!
  }

  @Override
  public void onResult(Result result) 
  {
	Log.v("CONSOLE","getValue() >>> "+ result.getValue());
	String code= result.getValue();
	
	if(code.equals(CodeConstant.CODE1))
	{
		//hideImages();
		Log.v("CONSOLE","1 getValue() >>> "+ result.getValue());
		//img1.setVisibility(View.VISIBLE);
		loadImage(1);
		return;
		
	}else if(code.equals(CodeConstant.CODE2))
	{
		//hideImages();
		Log.v("CONSOLE","2 getValue() >>> "+ result.getValue());
		//img2.setVisibility(View.VISIBLE);
		loadImage(2);
		return;
		
	}else if(code.equals(CodeConstant.CODE3))
	{
		//hideImages();
		Log.v("CONSOLE","3 getValue() >>> "+ result.getValue());
		//img3.setVisibility(View.VISIBLE);
		loadImage(3);
		return;
		
	}
  }
private void loadImage(int i) 
{
	// TODO Auto-generated method stub
	//img1.setImageDrawable(getResources().getDrawable(R.drawable.img1)):
	recycle();
	switch (i) {
		case 1:
			bmp= decodeSampledBitmapFromResource(getResources(), R.drawable.img_bg60, widthScreen, heightScreen);
			 btnExit.setVisibility(View.VISIBLE);
			 mScrollView.setVisibility(View.VISIBLE);
			break;
		case 2:
			bmp= decodeSampledBitmapFromResource(getResources(), R.drawable.img_bg60, widthScreen, heightScreen);
			btnExit.setVisibility(View.VISIBLE);
			mScrollView.setVisibility(View.VISIBLE);
			
			break;
		case 3:
			bmp= decodeSampledBitmapFromResource(getResources(), R.drawable.img_bg60, widthScreen, heightScreen);
		    btnExit.setVisibility(View.VISIBLE);
		    mScrollView.setVisibility(View.VISIBLE);
			break;
		default:
			break;
	}
	//if(bmp!=null) imageView.setImageBitmap(bmp);
	
}

	public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) 
	{
		// Raw height and width of image
		final int height = options.outHeight;
		final int width = options.outWidth;
		int inSampleSize = 1;
		
		if (height > reqHeight || width > reqWidth) {
		
		    final int halfHeight = height / 2;
		    final int halfWidth = width / 2;
		
		    // Calculate the largest inSampleSize value that is a power of 2 and keeps both
		    // height and width larger than the requested height and width.
		    while ((halfHeight / inSampleSize) > reqHeight
		            && (halfWidth / inSampleSize) > reqWidth) {
		        inSampleSize *= 2;
		    }
		}
		
		return inSampleSize;
	}
	public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
	        int reqWidth, int reqHeight) {

	    // First decode with inJustDecodeBounds=true to check dimensions
	    final BitmapFactory.Options options = new BitmapFactory.Options();
	    options.inJustDecodeBounds = true;
	    BitmapFactory.decodeResource(res, resId, options);

	    // Calculate inSampleSize
	    options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

	    // Decode bitmap with inSampleSize set
	    options.inJustDecodeBounds = false;
	    return BitmapFactory.decodeResource(res, resId, options);
	}

}
