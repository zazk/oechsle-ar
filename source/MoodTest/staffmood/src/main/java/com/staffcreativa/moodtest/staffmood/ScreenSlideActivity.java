/*
 * Copyright 2012 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.staffcreativa.moodtest.staffmood;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.hardware.Camera;
import android.os.Bundle;
import android.support.v13.app.FragmentStatePagerAdapter;


import android.support.v4.app.FragmentActivity;
import android.support.v4.app.NavUtils;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SurfaceHolder;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.io.IOException;


public class ScreenSlideActivity extends FragmentActivity   {



    //--- Pager ---------------------------------------
    /**
     * The number of pages (wizard steps) to show in this demo.
     */
    private static final int NUM_PAGES = 3;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    //-----------------------------

    //--- Camera Preview

    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout mFrame;

    //-----------------------------

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_screen_slide);


        //Pager ---------------------------------------

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        mPager.setPageTransformer(true, new OnBoardingPageTransformer(0.5f, 0.1f));
        //mPager.setRotation(90);




        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When changing pages, reset the action bar actions since they are dependent
                // on which page is currently active. An alternative approach is to have each
                // fragment expose actions itself (rather than the activity exposing actions),
                // but for simplicity, the activity provides the actions in this sample.
                invalidateOptionsMenu();
            }
        });


        // ---------------------------------------

        //Camera------------

        // Create an instance of Camera
        mCamera = getCameraInstance();


        mFrame = (FrameLayout) findViewById(R.id.background_frame_camera);

        // Create our Preview view and set it as the content of our activity.
        mPreview = new CameraPreview(this, mCamera);
        //mFrame.addView(mPreview);

        //----------------------
    }

    //Camera ------------------------

    public void logSensorData(View view)
    {
        Toast.makeText(getApplicationContext(), "Logged", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPause()
    {
        super.onPause();
        releaseCamera();

    }
    /** A safe way to get an instance of the Camera object. */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e){
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    private void releaseCamera(){
        if (mCamera != null){
            mCamera.release();        // release the camera for other applications
            mCamera = null;
        }
    }

    //-------------------------

    /**
     * A simple pager adapter that represents 5 {@link ScreenSlidePageFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class OnBoardingPageTransformer implements ViewPager.PageTransformer {
        float parallaxCoefficient;
        float distanceCoefficient;
        private static final float MIN_SCALE = 0.75f;

        public OnBoardingPageTransformer (float parallaxCoefficient, float distanceCoefficient) {
            this.parallaxCoefficient = parallaxCoefficient;
            this.distanceCoefficient = distanceCoefficient;

        }

        public void transformPage (View view, float position)
        {
            int pageWidth = view.getWidth();

            view.findViewById(R.id.background).setTranslationX(10f*position);

            view.findViewById(R.id.centerImage).setTranslationX(500f*position);



            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                //view.setAlpha(0);
                //view.findViewById(R.id.bigTitle).setRotation(50f*position);
                //view.findViewById(R.id.background).setTranslationX(0.2f*position);
                view.findViewById(R.id.precios).setTranslationY(0);
            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                //view.setAlpha(1);
                //view.setTranslationX(0);
                //view.setScaleX(1);
                //view.setScaleY(1);
                // title
                //view.findViewById(R.id.bigTitle).setRotation(10f);
                view.findViewById(R.id.precios).setTranslationY(0);
            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                //view.setAlpha(1 - position);

                // Counteract the default slide transition
                //view.setTranslationX(pageWidth * -position);
                //view.findViewById(R.id.bigTitle).setRotation(50f*position);
                // Scale the page down (between MIN_SCALE and 1)
                //float scaleFactor = MIN_SCALE
                //        + (1 - MIN_SCALE) * (1 - Math.abs(position));
                //view.setScaleX(scaleFactor);
                //view.setScaleY(scaleFactor);
                view.findViewById(R.id.precios).setVisibility(View.VISIBLE);
                view.findViewById(R.id.precios).setTranslationY(100f*position);
            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                //view.setAlpha(0);
                view.findViewById(R.id.precios).setVisibility(View.VISIBLE);
                view.findViewById(R.id.precios).setTranslationY(200f*position);
            }
        }
    }
}
