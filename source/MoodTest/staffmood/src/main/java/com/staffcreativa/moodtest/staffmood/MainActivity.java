package com.staffcreativa.moodtest.staffmood;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.VideoView;

import com.moodstocks.android.Scanner;
import com.moodstocks.android.MoodstocksError;

public class MainActivity extends Activity implements Scanner.SyncListener{

    // Moodstocks API key/secret pair
    private static final String API_KEY    = "irpphnsyvofeeimrqxfh";
    private static final String API_SECRET = "DeWBnXxBNiGFDVy6";

    private boolean compatible = false;
    private Scanner scanner;

    private Button btn_scan;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        compatible = Scanner.isCompatible();
        if (compatible) {
            Log.d("COMPAT", "TRUE");
            try {
                scanner = Scanner.get();
                String path = Scanner.pathFromFilesDir(this, "scanner.db");
                scanner.open(path, API_KEY, API_SECRET);
                // sync

                scanner.setSyncListener(this);
                scanner.sync();
            } catch (MoodstocksError e) {
                e.printStackTrace();
            }
        }


        btn_scan = (Button)findViewById(R.id.btn_scan);

        //Test Video--------------------------
        /*
        VideoView mVideo = (VideoView)findViewById(R.id.video_player_view);
        String path = "android.resource://" + getPackageName() + "/" + R.raw.video;

        Log.d("Moodstocks SDK", path  + ">>>>>>>>>>>>>>>>>>.");
        mVideo.setVideoURI(Uri.parse(path));
        mVideo.start();
        */
        //---------------------------
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void onScanButtonClicked(View view) {
        if (compatible) {
            startActivity(new Intent(this, ScanActivity.class));
        }
    }

    @Override
    public void onSyncStart() {
        Log.d("Moodstocks SDK", "Sync will start.");
    }

    @Override
    public void onSyncComplete() {
        try {
            Log.d("Moodstocks SDK", "Sync succeeded ("+ scanner.count() + " images)");
        } catch (MoodstocksError e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSyncFailed(MoodstocksError e) {
        Log.d("Moodstocks SDK", "Sync error #" + e.getErrorCode() + ": " + e.getMessage());
    }

    @Override
    public void onSyncProgress(int total, int current) {
        int percent = (int) ((float) current / (float) total * 100);
        Log.d("Moodstocks SDK", "Sync progressing: " + percent + "%");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (compatible) {
            try {
                scanner.close();
                scanner.destroy();
                scanner = null;
            } catch (MoodstocksError e) {
                Log.e("ERROR", "error");
                e.printStackTrace();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
