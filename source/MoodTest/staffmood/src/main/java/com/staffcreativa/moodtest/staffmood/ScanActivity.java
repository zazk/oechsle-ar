package com.staffcreativa.moodtest.staffmood;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v13.app.FragmentStatePagerAdapter;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.SurfaceView;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.VideoView;

import com.moodstocks.android.AutoScannerSession;
import com.moodstocks.android.Scanner;
import com.moodstocks.android.MoodstocksError;
import com.moodstocks.android.Result;
import com.staffcreativa.moodtest.utils.Constants;


public class ScanActivity extends FragmentActivity implements AutoScannerSession.Listener  {

    private AutoScannerSession session = null;
    private static final int TYPES = Result.Type.IMAGE | Result.Type.QRCODE | Result.Type.EAN13;


    //--- Pager ---------------------------------------
    private static final int NUM_PAGES = Constants.slides.length;

    /**
     * The pager widget, which handles animation and allows swiping horizontally to access previous
     * and next wizard steps.
     */
    private ViewPager mPager;

    /**
     * The pager adapter, which provides the pages to the view pager widget.
     */
    private PagerAdapter mPagerAdapter;

    //-----------------------------


    private FrameLayout mFrame;
    private FrameLayout mVideoFrame;
    private VideoView mVideo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        SurfaceView preview = (SurfaceView)findViewById(R.id.preview);

        try {
            session = new AutoScannerSession(this, Scanner.get(), this, preview);
            session.setResultTypes(TYPES);

        } catch (MoodstocksError e) {
            e.printStackTrace();
        }


        //Pager ---------------------------------------

        // Instantiate a ViewPager and a PagerAdapter.
        mPager = (ViewPager) findViewById(R.id.pager);
        //mPager.setPageTransformer(true, new OnBoardingPageTransformer(0.5f, 0.1f));

        mPagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager.setAdapter(mPagerAdapter);
        mPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
            mPager.setPageTransformer(true, new OnBoardingPageTransformer(0.5f, 0.1f, position));
            }
        });

        mFrame = (FrameLayout) findViewById(R.id.design);
        mVideoFrame = (FrameLayout) findViewById(R.id.video);
    }

    @Override
    protected void onResume() {
        super.onResume();
        session.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        session.stop();
    }

    @Override
    public void onResult(Result result) {


        Log.d("Moodstocks SDK", "GO GO GO. I found this shit!.");

        //mVideoFrame.bringToFront();

        mVideoFrame.setVisibility(View.VISIBLE);
        mVideo = (VideoView)findViewById(R.id.video_player_view);
        mVideo.setZOrderMediaOverlay ( true );
        String path = "android.resource://" + getPackageName() + "/" + R.raw.video;

        Log.d("Moodstocks SDK", path + ">>>>>>>>>>>>>>>>>>.");
        mVideo.setVideoURI(Uri.parse(path));
        mVideo.start();

        // video finish listener
        mVideo.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {

            @Override
            public void onCompletion(MediaPlayer mp) {

                Log.d("ON Completation Video", ">>>>>>>>>>>>>>>>>>." );


                mVideo.setVisibility(View.GONE);
                mVideoFrame.setVisibility(View.GONE);


                // Release when play video !!!!!!
                mFrame.setVisibility(View.VISIBLE);
                ObjectAnimator animator = ObjectAnimator.ofFloat(mFrame, View.ALPHA, 0f, 1f);
                animator.setDuration(1000); //ms
                animator.start();
                animator.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        //myButton.setVisibility(GONE);
                        mFrame.setVisibility(View.VISIBLE);
                    }
                });



            }
        });



        mVideoFrame.setVisibility(View.VISIBLE);


        /*



        */
        //mFrame.setVisibility(View.VISIBLE);
        /*
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setNeutralButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                session.resume();
            }
        });
        builder.setTitle(result.getType() == Result.Type.IMAGE ? "Image:" : "Barcode:");
        builder.setMessage(result.getValue());
        builder.show();*/
    }

    @Override
    public void onCameraOpenFailed(Exception e) {
        // Implemented in a few steps
    }

    @Override
    public void onWarning(String debugMessage) {
        // Implemented in a few steps
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.scan, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }


    /**
     * A simple pager adapter that represents 5 {@link ScreenSlidePageFragment} objects, in
     * sequence.
     */
    private class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            return ScreenSlidePageFragment.create(position);
        }

        @Override
        public int getCount() {
            return NUM_PAGES;
        }
    }

    public class OnBoardingPageTransformer implements ViewPager.PageTransformer {
        float parallaxCoefficient;
        float distanceCoefficient;
        int slide_position;
        private static final float MIN_SCALE = 0.75f;
        private int background;

        public OnBoardingPageTransformer (float parallaxCoefficient, float distanceCoefficient, int position) {
            this.parallaxCoefficient = parallaxCoefficient;
            this.distanceCoefficient = distanceCoefficient;
            this.slide_position = position;
        }

        public void transformPage (View view, float position)
        {
            int pageWidth = view.getWidth();

            view.findViewById(R.id.background).setTranslationX(10f*position);

            view.findViewById(R.id.centerImage).setTranslationX(500f*position);

            if (position < -1) { // [-Infinity,-1)
                // This page is way off-screen to the left.
                view.findViewById(R.id.precios).setTranslationY(0);

            } else if (position <= 0) { // [-1,0]
                // Use the default slide transition when moving to the left page
                view.findViewById(R.id.precios).setTranslationY(0);

            } else if (position <= 1) { // (0,1]
                // Fade the page out.
                view.findViewById(R.id.precios).setVisibility(View.VISIBLE);
                view.findViewById(R.id.precios).setTranslationY(100f*position);

            } else { // (1,+Infinity]
                // This page is way off-screen to the right.
                view.findViewById(R.id.precios).setVisibility(View.VISIBLE);
                view.findViewById(R.id.precios).setTranslationY(200f*position);
            }
        }
    }
}
