package com.staffcreativa.moodtest.staffmood;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.staffcreativa.moodtest.utils.Constants;

public class ScreenSlidePageFragment extends Fragment {
    /**
     * The argument key for the page number this fragment represents.
     */
    public static final String ARG_PAGE = "page";

    /**
     * The fragment's page number, which is set to the argument value for {@link #ARG_PAGE}.
     */
    private int mPageNumber;

    //
    public ImageView iv_background;
    /**
     * Factory method for this fragment class. Constructs a new fragment for the given page number.
     */
    public static ScreenSlidePageFragment create(int pageNumber) {
        ScreenSlidePageFragment fragment = new ScreenSlidePageFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PAGE, pageNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ScreenSlidePageFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPageNumber = getArguments().getInt(ARG_PAGE);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout containing a title and body text.
        ViewGroup rootView = (ViewGroup) inflater
                .inflate(R.layout.fragment_screen_slide_page, container, false);

        Log.d(Constants.APP_TAG, "mPageNumber: " + mPageNumber);

        //-- custom slide
        //Background
        int resID = getResources().getIdentifier("drawable/bg"+ mPageNumber , null , rootView.getContext().getPackageName());
        iv_background = (ImageView)rootView.findViewById(R.id.background);
        //iv_background.setImageResource(Constants.slides[mPageNumber]);
        iv_background.setImageResource(resID);


        //Precios
        resID = getResources().getIdentifier("drawable/precios"+ mPageNumber , null , rootView.getContext().getPackageName());
        iv_background = (ImageView)rootView.findViewById(R.id.precios);
        //iv_background.setImageResource(Constants.slides[mPageNumber]);
        iv_background.setImageResource(resID);

        return rootView;
    }

    public int getPageNumber() {
        return mPageNumber;
    }
}
