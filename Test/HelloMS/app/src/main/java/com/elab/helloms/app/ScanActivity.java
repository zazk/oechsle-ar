
package com.elab.helloms.app;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.SurfaceView;

import com.moodstocks.android.AutoScannerSession;
import com.moodstocks.android.Scanner;
import com.moodstocks.android.MoodstocksError;
import com.moodstocks.android.Result;

public class ScanActivity extends Activity implements AutoScannerSession.Listener {

    private AutoScannerSession session = null;
    private static final int TYPES = Result.Type.IMAGE | Result.Type.QRCODE | Result.Type.EAN13;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);

        SurfaceView preview = (SurfaceView)findViewById(R.id.preview);

        try {
            session = new AutoScannerSession(this, Scanner.get(), this, preview);
            session.setResultTypes(TYPES);
        } catch (MoodstocksError e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        session.start();
    }

    @Override
    protected void onPause() {
        super.onPause();
        session.stop();
    }
    
    @Override
    public void onCameraOpenFailed(Exception e) {
        // Implemented in a few steps
    }

    @Override
    public void onWarning(String debugMessage) {
        // Implemented in a few steps
    }

    @Override
    public void onResult(Result result) {
        // Implemented in a few steps
    }
}